"""
Last Modified 18|12|2021

Created by Daniel
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from numpy.random import random
from scipy.integrate import simps
from time import time

# generates N particles at a certain point
def genParticles(r, constants):
    """
    Generates N particles at a single point r (only 1d)

    INPUT:
    r (float): The point which all the particles will start at
    """
    # pulls some values from the constants dictionary
    # N is the number of particles
    N = constants['N']

    # returns N particles with position r
    return np.zeros(N) + r


# diffuses the particles
def diffuse(pos, constants):
    """
    Moves all the particles randomly

    INPUT:
    particles (list of positions ): contains coordinates for all N particles

    OUTPUT:
    shiftPos (list of positions ): new diffused positions of the particles
    """
    # the diffusion depends on the time step so pulls the time step from the constants dict
    dt = constants['dt']

    # shifts all the positions by a gaussian distribution
    shift = np.random.normal(0, 1, len(pos))*np.sqrt(dt)

    return pos+shift

# weight function
def weight(newpos, constants, oldpos):
    """
    Determines the "weights" of the new particle positions
    ultimately determines which new positions will be kept, and which ones will be deleted/duplicated

    INPUT:
    pos (array): positions of particles

    OUTPUT:
    weights (array): the weight of each particle position, essentially the same type of weights the metropolis algorithm uses
    """
    # pulls come constants from the dictionary
    E0 = constants['E0']
    dt = constants['dt']

    return np.exp(-dt*(V(newpos, constants) + V(oldpos, constants))/2 + dt*E0)

# determines the number of new positions that are kept and returns the "new" particles
def keepNewParticles(pos, constants):
    """
    Takes the initial positions of the particles, shifts them randomly, and determines how many of the new
    positions are kept (they can either be deleted or duplicated up to 2 times). 
    Then returns the new positions after taking into account how they are duplicated

    INPUT:
    pos (list of position arrays): initial position of the particles

    OUTPUT:
    newPos (list of position arrays, may be longer than original positions): collection of "new" particles after duplications and modifications
    """
    # shifts positions
    shiftPos = diffuse(pos, constants)

    # determines weights of new positions
    weights = weight(shiftPos, constants, pos)

    # m values determine which transitions are kept/duplicated and which ones are deleted
    # just takes all of the weights, determines the m value, then either takes the int part of m=3 or m=weight + u, whichever is smaller
    mVals = np.min([(weights + np.random.random(len(weights)) ).astype(int), (np.zeros(len(weights))+3).astype(int)], axis = 0)

    # will store the new positions
    newPos = []

    # gets rid of all the values which got an m value of 0
    shiftPos= shiftPos[mVals>0]
    # removes the 0 m Values
    mVals = mVals[mVals>0]

    # adds all the particles that have m =1
    newPos.extend(shiftPos[mVals == 1])

    # duplicates all the particles that have m = 2 and adds it to the newPos list
    newPos.extend(np.tile(shiftPos[mVals == 2], (2)))

    # duplicates all the particles that have m = 3 and adds them to the newpos list
    newPos.extend(np.tile(shiftPos[mVals == 3], (3)))

    # returns the new positions
    return newPos

# potential function for the problem
def V(r, constants):
    """
    Potential for which we are trying to solve the Schrodinger equation
    dimensionless 1d harmonic oscillator and Morse Oscillator Potentials are already filled out
    uncomment and comment the respective one

    INPUT:
    r (list of positions): list of positions of the particles

    OUTPUT:
    potentials (list of floats): list of potentials of the particles 
    """
    # ensures the position array is a numpy array not just a list
    r = np.asarray(r)

    # HARMONIC OSCILLATOR
    # return 0.5*r**2

    # MORSE OSCILLATOR
    V= 0.5*(np.exp(-2*r)-2*np.exp(-r))

    V = -1/r

    return V

def analSoln(x):
    """
    Analytical Solution to 1d Harmonic Oscillator and to Morse Oscillator (dimensionless)

    comment and uncomment the respective one for plotting after numerically solving
    """
    # HARMONIC OSCILLATOR
    # return np.pi**(-0.25)*np.exp(-x**2/2)

    # MORSE OSCILLATOR
    return np.sqrt(2)*np.exp(-np.exp(-x)-x/2)


# dictionary to hold a bunch of constants
# dt is the time step
# N is the number of particles being considered for this simulation
# steps is the number of monte carlo steps that are being done
# a is the constant for updating the trial energy
constants = {'dt': 0.01,'N':int(500), 'E0':0., 'steps': 1000, 'a':.1, 'Estep': 10}

# timing the runtime
start = time()

# stores the particle positions
particles = [genParticles(1., constants)]
# array to store the energies for a plot later
energies = []

# does the first perturbation
particles.append(keepNewParticles(particles[-1], constants))

# determines the new energy estimate (just the average potential energy of the particles)
energies.append(np.average(V(particles[-1], constants)))
# updates the trial energy in the constants
constants['E0'] = energies[-1]

# continues the steps once for the system to reach equilibrium
for j in range(constants['steps']):
    # one perturbation step
    # (diffuses particles -> annihilates and creates particles according to m values)
    particles.append(keepNewParticles(particles[-1], constants))

    # during the first set, the trial energy is updated only at every 'Estep' steps 
    if j% constants['Estep'] ==0:
        # determines new energy estimate

        # energy update with eq (16)
        energies.append(energies[0] + constants['a']*(1- len(particles[-1])/len(particles[0]) ))

        # energy update with eq (17)
        # energies.append( energies[0] + constants['a']*np.log(len(particles[0])/len(particles[-1])))

        # updates the value in the constants dictionary
        constants['E0'] = energies[-1]
    else:
        energies.append(constants['E0'])

# just prints to make sure the user knows that the first phase is completed without issue
print('Phase 1 Complete')

# the next set of steps is to collect points for the distribution
# this is under the assumption that the system has reached the ground state
wavefunction = []
for j in range(constants['steps']):
    # does a perturbation
    particles.append(keepNewParticles(particles[-1], constants))

    # determines new energy estimate

    # energy update with eq (16)
    energies.append(energies[0] + constants['a']*(1- len(particles[-1])/len(particles[0]) ))

    # energy update with eq (17)
    # energies.append( energies[0] + constants['a']*np.log(len(particles[0])/len(particles[-1])))

    # updates the value in the constants dictionary
    constants['E0'] = energies[-1]

    # adds the particle positions for the latest wavefunctions to the list that will be used to determine the ground state wave function
    wavefunction.extend(particles[-1])

# prints the final ground state energy
print('Ground State Energy: ', np.average(energies[-1000:]))
# prints the standard deviation of the trial energy during the second phase of the simulation
print('Ground State Energy Standard Deviation: ', np.std(energies[-1000:]))

print('Run time: ', time() - start)

# density = True ensures that the numerical wave function is normalized
hist, bins = np.histogram(wavefunction, bins = 200, density = True)
# used to plot the number of particles in each bin at the bin center instead of at the edges
binCenters = (bins[:-1]+bins[1:])/2

# normalizing the analytical solution
normAnal = simps(analSoln(binCenters), binCenters)

# plotting
plt.figure()

plt.subplot(2,1, 1)
plt.title('Ground State Wave Function')
plt.ylabel(r'$\varphi_0(x)$')
plt.xlabel('x')
# first plotting the numerical solution
plt.plot(binCenters, hist, color = 'blue', label = 'Numerical Solution')
# then the analytical solution
# plt.plot(binCenters, analSoln(binCenters)/normAnal, color = 'red', label = 'Analytical Solution')
plt.legend()

# plotting the trial energy at each step
plt.subplot(2,1, 2)
plt.title('Trial Energy during the Simulation')
plt.ylabel(r'Energy ($\hbar \omega$)')
plt.xlabel('Monte Carlo Step')
plt.plot(energies)

plt.tight_layout()
plt.show()