"""
Last Modified 18|12|2021

Created by Daniel
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from numpy.random import random
from scipy.integrate import simps
from time import time

# generates N particles at a certain point
def genParticles(r, constants):
    """
    Generates N particles at a single point r (any dimension)

    INPUT:
    r (array): The point which all the particles will start at
    """
    # pulls some values from the constants dictionary
    # N is the number of particles
    N = constants['N']
    # dim is the dimension of the problem
    dim = constants['dim']

    # returns N particles with position r
    return np.zeros((N, dim)) + r


# diffuses the particles
def diffuse(pos, constants):
    """
    Moves all the particles randomly

    INPUT:
    particles (list of positions ): contains coordinates for all N particles

    OUTPUT:
    shiftPos (list of positions ): new diffused positions of the particles
    """
    # the diffusion depends on the time step so pulls the time step from the constants dict
    dt = constants['dt']
    
    dim = constants['dim']

    # shifts all the positions by a gaussian distribution
    shift = np.random.normal(0, 1, size = (len(pos), dim) )*np.sqrt(dt)


    return pos+shift

# weight function
def weight(newpos, constants, oldpos):
    """
    Determines the "weights" of the new particle positions
    ultimately determines which new positions will be kept, and which ones will be deleted/duplicated

    INPUT:
    pos (array): positions of particles

    OUTPUT:
    weights (array): the weight of each particle position, essentially the same type of weights the metropolis algorithm uses
    """
    # pulls come constants from the dictionary
    E0 = constants['E0']
    dt = constants['dt']



    newV = V(newpos, constants)
    oldV = V(oldpos, constants)

    return np.exp(-dt*( newV + oldV)/2 + dt*E0)

# determines the number of new positions that are kept and returns the "new" particles
def keepNewParticles(pos, constants):
    """
    Takes the initial positions of the particles, shifts them randomly, and determines how many of the new
    positions are kept (they can either be deleted or duplicated up to 2 times). 
    Then returns the new positions after taking into account how they are duplicated

    INPUT:
    pos (list of position arrays): initial position of the particles

    OUTPUT:
    newPos (list of position arrays, may be longer than original positions): collection of "new" particles after duplications and modifications
    """
    # shifts positions
    shiftPos = diffuse(pos, constants)

    # determines weights of new positions
    weights = weight(shiftPos, constants, pos)

    # m values determine which transitions are kept/duplicated and which ones are deleted
    # just takes all of the weights, determines the m value, then either takes the int part of m=3 or m=weight + u, whichever is smaller
    mVals = np.min([(weights + np.random.random(len(weights)) ).astype(int), (np.zeros(len(weights))+3).astype(int)], axis = 0)

    # will store the new positions
    newPos = []

    # gets rid of all the values which got an m value of 0
    shiftPos= shiftPos[mVals>0]
    # removes the 0 m Values
    mVals = mVals[mVals>0]


    # adds all the particles that have m =1
    newPos.extend(shiftPos[mVals == 1])

    # duplicates all the particles that have m = 2 and adds it to the newPos list
    newPos.extend(np.tile(shiftPos[mVals == 2], (2,1)))

    # duplicates all the particles that have m = 3 and adds them to the newpos list
    newPos.extend(np.tile(shiftPos[mVals == 3], (3,1)))

    # returns the new positions
    return newPos

# potential function for the problem
def V(r, constants):
    """
    Potential for which we are trying to solve the Schrodinger equation

    Already has:
    Nd symmetric harmonic oscillator
    Nd symmetric morse oscillator
    Nd square potential
    Nd coulomb potential

    comment and uncomment as required

    INPUT:
    r (list of positions): list of positions of the particles

    OUTPUT:
    potentials (list of floats): list of potentials of the particles 
    """
    # stores the potential values for each particle
    V = []

    # Kd SYMMETRIC HARMONIC OSCILLATOR
    # transposes the positions array so it is only made up of K arrays, one for each dimension of the problem
    r = np.asarray(r).T
    for coordinate in r:
        V.append(0.5*coordinate**2)
    # sums the potentials for each of the coordinates
    V = np.sum(V, axis = 0)
    # this gives an array of each of the potentials for each particle

    # # Kd SYMMETRIC MORSE OSCILLATOR
    # # transposes the positions array so it is only made up of K arrays, one for each dimension of the problem
    # r = np.asarray(r).T
    # for coordinate in r:
    #     V.append( 0.5*(np.exp(-2*coordinate)-2*np.exp(-coordinate)) )
    # # sums the potentials for each of the coordinates
    # V = np.sum(V, axis = 0)
    # # this gives an array of each of the potentials for each particle

    # # Kd SYMMETRIC POTENTIAL WELL
    # # transposes the positions array so it is only made up of K arrays, one for each dimension of the problem
    # r = np.asarray(r).T
    # for coordinate in r:
    #     temp = np.zeros(len(coordinate))
    #     temp[np.abs(coordinate) > 1] = 100
    #     V.append(temp)
    # # sums the potentials for each of the coordinates
    # V = np.sum(V, axis = 0)
    # # this gives an array of each of the potentials for each particle

    # # Coulomb potential
    # V = (-1/np.linalg.norm(r, axis = 1))

    return V


# dictionary to hold a bunch of constants
# dt is the time step
# N is the number of particles being considered for this simulation
# steps is the number of monte carlo steps that are being done
# a is the constant for updating the trial energy
# dim is the dimension of the problem
constants = {'dt': 0.01,'N':int(1000), 'E0':0., 'steps': 5000, 'a':1., 'Estep': 10, 'dim': 2}

# timing the runtime
start = time()

# stores the particle positions
particles = [genParticles([0., 0.], constants)]
# array to store the energies for a plot later
energies = []

# does the first perturbation
particles.append(keepNewParticles(particles[-1], constants))

# determines the new energy estimate (just the average potential energy of the particles)
energies.append(np.average(V(particles[-1], constants)))
# updates the trial energy in the constants dict
constants['E0'] = energies[-1]

# continues the steps once for the system to reach equilibrium
for j in range(constants['steps']):
    # one perturbation step
    # (diffuses particles -> annihilates and creates particles according to m values)
    particles.append(keepNewParticles(particles[-1], constants))

    # during the first set, the trial energy is updated only at every 'Estep' steps 
    if j% constants['Estep'] ==0:
        # determines new energy estimate

        # energy update with eq (16)
        # energies.append(energies[0] + constants['a']*(1- len(particles[-1])/len(particles[0]) ))

        # energy update with eq (17)
        energies.append( energies[0] + constants['a']*np.log(len(particles[0])/len(particles[-1])))

        # updates the value in the constants dictionary
        constants['E0'] = energies[-1]
    # just appends the previous energy to the list if it isn't updated so that the energy plot is accurate
    else:
        energies.append(constants['E0'])

# just prints to make sure the user knows that the first phase is completed without issue
print('Phase 1 Complete')

# the next set of steps is to collect points for the distribution
# this is under the assumption that the system has reached the ground state
wavefunction = []
for j in range(constants['steps']):
    # does a perturbation
    particles.append(keepNewParticles(particles[-1], constants))

    # determines new energy estimate

    # energy update with eq (16)
    # energies.append(energies[0] + constants['a']*(1- len(particles[-1])/len(particles[0]) ))

    # energy update with eq (17)
    energies.append( energies[0] + constants['a']*np.log(len(particles[0])/len(particles[-1])))

    # updates the value in the constants dictionary
    constants['E0'] = energies[-1]

    # adds the particle positions for the latest wavefunctions to the list that will be used to determine the ground state wave function
    wavefunction.extend(particles[-1])

# prints the final ground state energy
print('Ground State Energy: ', np.average(energies[-1000:]))
# prints the standard deviation of the trial energy during the second phase of the simulation
print('Ground State Energy Standard Deviation: ', np.std(energies[-1000:]))

print('Run time: ', time() - start)

wavefunction = np.array(wavefunction)
# density = True ensures that the numerical wave function is normalized
hist, bins = np.histogramdd(wavefunction, bins = 100, density = True)
# gets the bin centers for each dimension
binCenters = []
for dimension in bins:
    binCenters.append((dimension[:-1] + dimension[1:])/2)

# creates a meshgrid for the dimensions
grid = np.meshgrid(*binCenters)

# # normalizing the analytical solution
# normAnal = simps(analSoln(binCenters), binCenters)

# plotting
fig = plt.figure()
ax = plt.axes(projection = '3d')

ax.set_title('Ground State Wave Function')
ax.set_zlabel(r'$\varphi_0(x)$')
ax.set_ylabel('y')
ax.set_xlabel('x')
# plotting potential, this makes the wave function plot 2d though so I commented it out by default
# b = ax.plot_surface(*grid, V(np.array(grid).T, constants), cmap = 'Greys', alpha = 0.1, label = 'Potential')
# plotting the wave function
# if the dimension of the problem is 3, then we need to have a colour mapped scatter plot with the points thinned out
if constants['dim'] == 3:
    # sets random numbers in the histogram to 0 to thin out the number of points
    hist = np.random.choice([0,1], size = np.shape(hist))*hist

    # determines the threshold histogram density for the point to appear on the plot
    # once again to thin out the number of points so that the plot looks fine
    histFilter = 0.15

    # removing the 0 histogram points and the points below the filter
    for i in range(len(grid)):
        grid[i] = grid[i][hist > histFilter]
    hist = hist[hist> histFilter]

    # scatter plotting
    a = ax.scatter(*grid, c = hist, cmap = 'magma', alpha = 0.05)

    cbar = plt.colorbar(a)
    cbar.set_label(r'Wave Function Value')
# if it is 2d then it does a regular surface plot
elif constants['dim'] == 2:

    a = ax.plot_surface(*grid, hist, cmap = 'magma', label = 'Wave Function')

    cbar = plt.colorbar(a)
    cbar.set_label(r'Wave Function Value')
# otherwise converts it to a 2d plot
else:
    plt.clf()
    plt.title('Ground State Wave Function')
    plt.ylabel(r'$\varphi_0(x)$')
    plt.xlabel('x')
    plt.plot(*grid, hist)


plt.tight_layout()
plt.show()